cmake_minimum_required (VERSION 3.9)

project (StlAlgorithms VERSION 0.1.0 LANGUAGES CXX)

add_subdirectory (external/google/googletest EXCLUDE_FROM_ALL)

add_executable (algos algos.cpp)

target_compile_features (algos PRIVATE cxx_std_17)

target_link_libraries (algos PRIVATE gtest gtest_main)

enable_testing ()
add_test (algos ${CMAKE_CURRENT_BINARY_DIR}/algos)