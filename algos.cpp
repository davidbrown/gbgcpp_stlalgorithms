#include <gtest/gtest.h>

#include <algorithm>
#include <string>
#include <sstream>
#include <vector>

class ExerciseOne : public ::testing::Test
{
protected:
    std::vector<int> const sequence {1, 50, 3, 9, 18};
};

TEST_F(ExerciseOne, Find_WillFindFirstElementDivisibleByFive)
{
    auto const element = std::find_if(std::cbegin(sequence),
                                      std::cend(sequence),
                                      [](auto x) {return x % 5 == 0;});

    EXPECT_EQ(std::cbegin(sequence) + 1, element);
}

class ExerciseTwo : public ::testing::Test
{
protected:
    std::string const string {"algorithms are fun"};
};

TEST_F(ExerciseTwo, Search_WillFindSubstring)
{
    auto const sub_string = std::string{"are"};

    auto const pos = std::search(std::cbegin(string),
                                 std::cend(string),
                                 std::cbegin(sub_string),
                                 std::cend(sub_string));

    EXPECT_NE(std::cend(string), pos);
}

class ExerciseThree : public ::testing::Test
{
protected:
    std::vector<int> const sequence {1, 200, 5, 7, 900};
};

TEST_F(ExerciseThree, CopyIf_ElementIsGreaterThanHundred)
{
    auto result = std::vector<int>{};

    std::copy_if(std::cbegin(sequence),
                 std::cend(sequence),
                 std::back_inserter(result),
                 [](auto x){ return x > 100;});

    EXPECT_EQ(2, result.size());

    auto const expected = std::vector<int>{200, 900};
    EXPECT_EQ(expected, result);
}

TEST_F(ExerciseThree, Copy_ElementsToOstream)
{
    auto out = std::ostringstream{};

    auto const expected = std::string{"1 200 5 7 900 "};

    std::copy(std::cbegin(sequence),
              std::cend(sequence),
              std::ostream_iterator<int>(out, " "));

    EXPECT_EQ(expected, out.str());
}

class ExerciseFour : public ::testing::Test
{
protected:
    std::vector<int> const sequence {2, 3, 4, 5};
};

TEST_F(ExerciseFour, Transform_WillDoubleAllElementsInContainer)
{
    auto result = std::vector<int>{};

    // Not guaranteed to apply unary operation in-order. I have checked the
    // implementation for libstdc++ and it does apply everything in order...
    std::transform(std::cbegin(sequence),
                   std::cend(sequence),
                   std::back_inserter(result),
                   [](auto x) -> int {return x * 2;});

    auto const expected = std::vector<int>{4, 6, 8, 10};
    EXPECT_EQ(expected, result);
}

class ExerciseFive : public ::testing::Test
{
protected:
    std::string string {"aaccdffgfgfgiili"};
};

TEST_F(ExerciseFive, Sort_WillSortString)
{
    auto const expected = std::string{"aaccdffffgggiiil"};

    std::sort(std::begin(string), std::end(string));

    EXPECT_EQ(expected, string);
}

TEST_F(ExerciseFive, Unique_WillPartitionSortedString)
{
    auto const expected = std::string{"acdfgil"};

    std::sort(std::begin(string), std::end(string));
    auto partition = std::unique(std::begin(string), std::end(string));

    auto distance = std::distance(std::begin(string), partition);
    auto result = std::string{string.substr(0, distance)};

    EXPECT_EQ(expected, result);
}

TEST_F(ExerciseFive, Erase_WillRemovePartionedElements)
{
    auto const expected = std::string{"acdfgil"};

    std::sort(std::begin(string), std::end(string));
    auto partition = std::unique(std::begin(string), std::end(string));
    string.erase(partition, string.end());

    EXPECT_EQ(expected, string);
}

class ExerciseSix : public ::testing::Test
{
protected:
    std::vector<int> sequence {1, 500, 345, 6, 3, 6};
};

TEST_F(ExerciseSix, StablePartition_WillPartitionAndLeaveOrderIntact)
{
    auto const expected = std::vector<int>{500, 345, 1, 6, 3, 6};

    std::stable_partition(std::begin(sequence),
                          std::end(sequence),
                          [](auto x){ return x > 100;});

    EXPECT_EQ(expected, sequence);
}

class ExerciseSeven : public ::testing::Test
{
protected:
    std::vector<int> const sequence {1, 500, 345, 6, 3, 6};
};

TEST_F(ExerciseSeven, CountIf_WillCountNumberOfElementsGreaterThanOneHundred)
{
    auto const count = std::count_if(std::cbegin(sequence),
                                     std::cend(sequence),
                                     [](auto x){ return x > 100;});

    EXPECT_EQ(2, count);
}
